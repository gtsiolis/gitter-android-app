package im.gitter.gitter.fragments;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import im.gitter.gitter.PeopleAdapter;
import im.gitter.gitter.R;
import im.gitter.gitter.activities.MainActivity;
import im.gitter.gitter.content.ContentProvider;
import im.gitter.gitter.content.RestResponseReceiver;
import im.gitter.gitter.content.RestServiceHelper;
import im.gitter.gitter.models.Room;

public class PeopleFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, SwipeRefreshLayout.OnRefreshListener {

    private RestResponseReceiver restResponseReceiver = new RestResponseReceiver();
    private SwipeRefreshLayout swipeLayout;
    private RecyclerView recyclerView;
    private PeopleAdapter peopleAdapter;
    private View emptyView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        peopleAdapter = new PeopleAdapter(getActivity());

        getActivity().registerReceiver(restResponseReceiver, restResponseReceiver.getFilter());

        setHasOptionsMenu(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_people, container, false);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);
        swipeLayout.setColorSchemeResources(R.color.caribbean, R.color.jaffa, R.color.ruby);

        recyclerView = (RecyclerView) view.findViewById(R.id.list_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(peopleAdapter);

        emptyView = view.findViewById(R.id.empty_view);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

        getActivity().setTitle(R.string.people);

        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onResume() {
        super.onResume();

        RestServiceHelper.getInstance().getRoomList(getActivity());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                ((MainActivity) getActivity()).openDrawer();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        getActivity().unregisterReceiver(restResponseReceiver);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String sortOrder =
                Room.MENTIONS + " DESC, " +
                Room.UNREAD_ITEMS + " DESC, " +
                Room.LAST_ACCESS_TIME + " DESC";
        return new CursorLoader(getActivity(), ContentProvider.ROOMS_CONTENT_URI, null, Room.ONE_TO_ONE, null, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        boolean hasNoResults = cursor == null || cursor.getCount() == 0;
        emptyView.setVisibility(hasNoResults ? View.VISIBLE : View.GONE);
        peopleAdapter.setCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        peopleAdapter.setCursor(null);
    }

    @Override
    public void onRefresh() {
        long requestId = RestServiceHelper.getInstance().getRoomList(getActivity());
        restResponseReceiver.listen(requestId, new RestResponseReceiver.Listener() {
            @Override
            public void onResponse(int statusCode) {
                swipeLayout.setRefreshing(false);
            }
        });
    }
}
