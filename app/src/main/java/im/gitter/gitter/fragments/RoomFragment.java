package im.gitter.gitter.fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import im.gitter.gitter.LoginData;
import im.gitter.gitter.NotificationsDialog;
import im.gitter.gitter.R;
import im.gitter.gitter.UserAgent;
import im.gitter.gitter.activities.MainActivity;
import im.gitter.gitter.content.ContentProvider;
import im.gitter.gitter.content.RestServiceHelper;
import im.gitter.gitter.models.Room;
import im.gitter.gitter.network.Api;
import im.gitter.gitter.content.RestResponseReceiver;

public class RoomFragment extends Fragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = RoomFragment.class.getSimpleName();
    private static final int LOADER_ID = TAG.hashCode();
    private String roomId;
    private View view;
    private WebView webView;
    private RequestQueue requestQueue;
    private FloatingActionButton joinButton;
    private ShareActionProvider shareActionProvider;
    private Intent shareIntent;
    private Cursor cursor;
    private RestServiceHelper rest;
    private RestResponseReceiver restResponseReceiver;

    private LruCache<String, WebView> webViewCache;
    private FrameLayout webViewWrapper;

    public static RoomFragment newInstance(String roomId) {
        RoomFragment fragment = new RoomFragment();

        Bundle args = new Bundle();
        args.putString("roomId", roomId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        roomId = args.containsKey("roomId") ? args.getString("roomId") : savedInstanceState.getString("roomId");

        if (roomId == null) {
            throw new IllegalArgumentException("roomId required");
        }

        webViewCache = ((MainActivity) getActivity()).getWebViewCache();
        rest = RestServiceHelper.getInstance();
        restResponseReceiver = new RestResponseReceiver();

        long requestId = rest.getRoom(getActivity(), roomId);
        restResponseReceiver.listen(requestId, new RestResponseReceiver.FailureListener() {
            @Override
            public void onFailure(int statusCode) {
                Toast.makeText(
                        getActivity(),
                        R.string.network_failed,
                        Toast.LENGTH_SHORT
                ).show();
            }
        });

        requestQueue = Volley.newRequestQueue(getActivity());
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_room, container, false);

            final CircularProgressView loadingSpinner = (CircularProgressView) view.findViewById(R.id.loading_spinner);
            joinButton = (FloatingActionButton) view.findViewById(R.id.join_button);
            joinButton.setOnClickListener(this);

            final String chatPageUrl = "file:///android_asset/www/mobile/embedded-chat.html?troupeId=" + roomId;
            final String accessToken = new LoginData(getActivity()).getAccessToken();

            webViewWrapper = (FrameLayout) view.findViewById(R.id.web_view_wrapper);
            WebView cachedWebView = webViewCache.get(roomId);
            if (cachedWebView != null) {
                webViewWrapper.removeAllViews();
                webViewWrapper.addView(cachedWebView);
                webView = cachedWebView;
            } else {
                webView = (WebView) view.findViewById(R.id.web_view);
                webViewCache.put(roomId, webView);
            }

            WebSettings settings = webView.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setDomStorageEnabled(true);
            settings.setBuiltInZoomControls(true);
            settings.setDisplayZoomControls(false);
            settings.setAllowUniversalAccessFromFileURLs(true);
            settings.setUserAgentString(UserAgent.addGitterDetails(getActivity(), settings.getUserAgentString()));

            webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url.equals(chatPageUrl)) {
                        // just a refresh, let the webview handle it (could be some faye reset stuff)
                        return false;
                    } else {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        return true;
                    }
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    loadingSpinner.setVisibility(View.VISIBLE);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    loadingSpinner.setVisibility(View.GONE);
                    webView.evaluateJavascript("window.bearerToken = '" + accessToken + "';", null);
                }
            });

            if (!chatPageUrl.equals(webView.getUrl())) {
                webView.loadUrl(chatPageUrl);
            } else {
                loadingSpinner.setVisibility(View.GONE);
            }
        }
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // we have to wait for the activity to create the action bar
        ((MainActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(null);

        getActivity().setTitle(null);

        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.room, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (cursor != null && cursor.moveToFirst()) {
            Room room = Room.newInstance(cursor);

            MenuItem shareMenu = menu.findItem(R.id.share);
            shareMenu.setVisible(!room.isOneToOne());
            shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareMenu);
            if (shareIntent != null) {
                shareActionProvider.setShareIntent(shareIntent);
            }

            menu.findItem(R.id.mark_all_read).setVisible(room.isRoomMember());
            menu.findItem(R.id.notifications).setVisible(room.isRoomMember());
            // cant leave one to ones
            menu.findItem(R.id.leave_room).setVisible(!room.isOneToOne() && room.isRoomMember());
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // blur event triggers eyeballs off
        webView.evaluateJavascript("window.dispatchEvent(new Event('blur'));", null);

        getActivity().unregisterReceiver(restResponseReceiver);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("roomId", roomId);
    }

    @Override
    public void onResume() {
        super.onResume();

        webView.onResume();

        // focus event triggers eyeballs on
        webView.evaluateJavascript("window.dispatchEvent(new Event('focus'));", null);
        getActivity().registerReceiver(restResponseReceiver, new IntentFilter(RestServiceHelper.INTENT_ACTION));
    }

    @Override
    public void onStop() {
        super.onStop();

        webView.onPause();

        if (requestQueue != null) {
            requestQueue.cancelAll(this);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (view != null) {
            webViewWrapper.removeAllViews();
            webView.setWebViewClient(null);
            ViewGroup parentViewGroup = (ViewGroup) view.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                // technically we should be going up the heirarchy instead of popping the back stack,
                // but from out point of view it is the same thing.
                getActivity().onBackPressed();
                return true;
            case R.id.mark_all_read:
                markAllMessagesRead();
                return true;
            case R.id.notifications:
                new NotificationsDialog(getActivity(), roomId).show();
                return true;
            case R.id.leave_room:
                createLeaveRoomDialog().show();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private AlertDialog createLeaveRoomDialog() {
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.leave_alert_title)
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // do nothing
                    }
                })
                .setPositiveButton(R.string.leave_alert_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        rest.leaveRoom(getActivity(), roomId);
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.putExtra(MainActivity.GO_TO_ALL_CONVERSATIONS_INTENT_KEY, true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        getActivity().onBackPressed();
                    }
                })
                .create();
    }

    private void markAllMessagesRead() {
        final Api api = new Api(getActivity());
        requestQueue.add(api.createJsonObjRequest(
                Request.Method.DELETE,
                "/v1/user/me/rooms/" + roomId + "/unreadItems/all",
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {}
                },
                null
        ));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.join_button) {
            long requestId = rest.joinRoom(getActivity(), roomId);
            restResponseReceiver.listen(requestId, new RestResponseReceiver.SuccessFailureListener() {
                @Override
                public void onSuccess() {
                    Room room = Room.newInstance(cursor);
                    Toast.makeText(
                            getActivity(),
                            getString(R.string.join_welcome, room.getName()),
                            Toast.LENGTH_LONG
                    ).show();
                }

                @Override
                public void onFailure(int statusCode) {
                    Toast.makeText(
                            getActivity(),
                            R.string.join_failed,
                            Toast.LENGTH_SHORT
                    ).show();
                }
            });
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), Uri.withAppendedPath(ContentProvider.ROOMS_CONTENT_URI, roomId), null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data.moveToFirst()) {
            Room room = Room.newInstance(data);

            // we take advantage of the fact that onLoadFinished is called onResume in order to track page views
            trackPageView("/" + room.getUri());

            if (this.cursor != data) {
                this.cursor = data;

                getActivity().setTitle(room.getName());
                joinButton.setVisibility(room.isRoomMember() ? View.GONE : View.VISIBLE);

                shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_message, "https://gitter.im/" + room.getUri()));
                if (shareActionProvider != null) {
                    shareActionProvider.setShareIntent(shareIntent);
                }

                // triggers onPrepareOptionsMenu
                getActivity().invalidateOptionsMenu();
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        cursor = null;
    }

    private void trackPageView(String page) {
        Log.i(TAG, "tracking page view for " + page);
        try {
            JSONObject props = new JSONObject();
            props.put("pageName", page);
            props.put("authenticated", true);
            props.put("isUserHome", false);
            props.put("isCommunityPage", false);
            props.put("userAgent", UserAgent.get(getActivity()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
