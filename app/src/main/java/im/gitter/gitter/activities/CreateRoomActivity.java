package im.gitter.gitter.activities;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import im.gitter.gitter.R;
import im.gitter.gitter.content.ContentProvider;
import im.gitter.gitter.content.RestResponseReceiver;
import im.gitter.gitter.content.RestServiceHelper;
import im.gitter.gitter.fragments.PickCommunityDialog;
import im.gitter.gitter.fragments.PrivacyDialog;
import im.gitter.gitter.fragments.RoomNameDialog;
import im.gitter.gitter.models.CreateRoomModel;
import im.gitter.gitter.models.Group;
import im.gitter.gitter.models.Repo;
import im.gitter.gitter.network.ApiRequest;
import im.gitter.gitter.network.VolleySingleton;
import im.gitter.gitter.utils.CursorUtils;

public class CreateRoomActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, CreateRoomModel.Listener, View.OnClickListener, PickCommunityDialog.Delegate, RoomNameDialog.Delegate, PrivacyDialog.Delegate
{
    public static final String RESULT_EXTRA_COMMUNITY_REQUIRED = "communityRequired";
    public static final String RESULT_EXTRA_ROOM_ID = "roomId";

    private ArrayAdapter<Group> communityAdapter;
    private CreateRoomModel model = new CreateRoomModel();
    private RequestQueue requestQueue;

    private boolean isSubmitting = false;

    private View rootView;
    private Snackbar creatingSnackbar;

    private RelativeLayout communitySection;
    private TextView communityDescription;

    private RelativeLayout roomNameSection;
    private TextView roomNameDescription;

    private RelativeLayout privacySection;
    private TextView privacyDescription;

    private RelativeLayout orgCanJoinSection;
    private TextView orgCanJoinTitle;
    private SwitchCompat orgCanJoinSwitch;

    private RelativeLayout githubOnlySection;
    private SwitchCompat githubOnlySwitch;

    private RelativeLayout addBadgeSection;
    private TextView addBadgeDescription;
    private SwitchCompat addBadgeSwitch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        communityAdapter = new ArrayAdapter<>(this, android.support.design.R.layout.select_dialog_singlechoice_material);
        model.setListener(this);
        requestQueue = VolleySingleton.getInstance(this).getRequestQueue();

        setTitle(R.string.create_room_title);

        setContentView(R.layout.activity_create_room);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);

        rootView = findViewById(android.R.id.content);
        creatingSnackbar = Snackbar.make(rootView, R.string.create_room_creating, Snackbar.LENGTH_INDEFINITE);

        communitySection = (RelativeLayout) findViewById(R.id.community_section);
        communitySection.setOnClickListener(this);
        communityDescription = (TextView) findViewById(R.id.community_description);

        roomNameSection = (RelativeLayout) findViewById(R.id.room_name_section);
        roomNameSection.setOnClickListener(this);
        roomNameDescription = (TextView) findViewById(R.id.room_name_descrition);

        privacySection = (RelativeLayout) findViewById(R.id.privacy_section);
        privacySection.setOnClickListener(this);
        privacyDescription = (TextView) findViewById(R.id.privacy_description);

        orgCanJoinSection = (RelativeLayout) findViewById(R.id.org_can_join_section);
        orgCanJoinSection.setOnClickListener(this);
        orgCanJoinTitle = (TextView) findViewById(R.id.org_can_join_title);
        orgCanJoinSwitch = (SwitchCompat) findViewById(R.id.org_can_join_switch);
        orgCanJoinSwitch.setOnClickListener(this);

        githubOnlySection = (RelativeLayout) findViewById(R.id.github_only_section);
        githubOnlySection.setOnClickListener(this);
        githubOnlySwitch = (SwitchCompat) findViewById(R.id.github_only_switch);
        githubOnlySwitch.setOnClickListener(this);

        addBadgeSection = (RelativeLayout) findViewById(R.id.add_badge_section);
        addBadgeSection.setOnClickListener(this);
        addBadgeDescription = (TextView) findViewById(R.id.add_badge_description);
        addBadgeSwitch = (SwitchCompat) findViewById(R.id.add_badge_switch);
        addBadgeSwitch.setOnClickListener(this);

        onChange(model);

        getLoaderManager().initLoader(0, null, this);
        RestServiceHelper.getInstance().getAdminGroups(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_room, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.create).setEnabled(!isSubmitting);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                setResult(Activity.RESULT_CANCELED);
                finish();
                return true;
            case R.id.create:
                submit();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, ContentProvider.ADMIN_GROUPS_CONTENT_URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        communityAdapter.clear();

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Group group = Group.newInstance(CursorUtils.getContentValues(cursor));
            communityAdapter.add(group);
        }
        communityAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {}

    @Override
    public void onChange(CreateRoomModel model) {
        Group community = model.getCommunity();
        String roomName = model.getRoomName();
        Repo linkedRepo = model.getLinkedRepo();

        if (community == null) {
            communityDescription.setText(R.string.required_field);
        } else {
            communityDescription.setText(getString(R.string.create_room_community_name_with_uri, community.getName(), community.getUri()));
        }

        if (roomName == null) {
            roomNameDescription.setText(R.string.required_field);
        } else if (linkedRepo != null) {
            roomNameDescription.setText(getString(R.string.create_room_linked_room_name, roomName, linkedRepo));
        } else {
            roomNameDescription.setText(roomName);
        }

        // There are nicer ways for styling disabled views, but this is the easiest
        if (model.isPublicOptionEnabled() && model.isPrivateOptionEnabled()) {
            privacySection.setEnabled(true);
            privacySection.setAlpha(1f);
        } else {
            privacySection.setEnabled(false);
            privacySection.setAlpha(0.5f);
        }

        if (model.isPrivate()) {
            privacyDescription.setText(R.string.create_room_private_with_description);
        } else {
            privacyDescription.setText(R.string.create_room_public_with_description);
        }

        orgCanJoinSection.setVisibility(model.isOrgCanJoinEnabled() ? View.VISIBLE : View.GONE);
        if (community != null) {
            orgCanJoinTitle.setText(getString(R.string.create_room_org_can_join, community.getGithubGroupName()));
        }
        orgCanJoinSwitch.setChecked(model.isOrgCanJoin());

        githubOnlySection.setVisibility(model.isGithubOnlyEnabled() ? View.VISIBLE : View.GONE);
        githubOnlySwitch.setChecked(model.isGithubOnly());

        addBadgeSection.setVisibility(model.isAddBadgeEnabled() ? View.VISIBLE : View.GONE);
        if (linkedRepo != null) {
            addBadgeDescription.setText(getString(R.string.create_room_add_badge_description, linkedRepo.getUri()));
        }
        addBadgeSwitch.setChecked(model.isAddBadge());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.community_section:
                new PickCommunityDialog().show(getFragmentManager(), null);
                break;
            case R.id.room_name_section:
                new RoomNameDialog().show(getFragmentManager(), null);
                break;
            case R.id.privacy_section:
                new PrivacyDialog().show(getFragmentManager(), null);
                break;
            case R.id.org_can_join_section:
                model.setOrgCanJoin(!model.isOrgCanJoin());
                break;
            case R.id.org_can_join_switch:
                model.setOrgCanJoin(orgCanJoinSwitch.isChecked());
                break;
            case R.id.github_only_section:
                model.setGithubOnly(!model.isGithubOnly());
                break;
            case R.id.github_only_switch:
                model.setGithubOnly(githubOnlySwitch.isChecked());
                break;
            case R.id.add_badge_section:
                model.setAddBadge(!model.isAddBadge());
                break;
            case R.id.add_badge_switch:
                model.setAddBadge(addBadgeSwitch.isChecked());
                break;
        }
    }

    @Override
    public void onCreateCommunitySelected(PickCommunityDialog fragment) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(RESULT_EXTRA_COMMUNITY_REQUIRED, true);
        setResult(Activity.RESULT_CANCELED, resultIntent);
        finish();
    }

    @Override
    public CreateRoomModel getModel(RoomNameDialog dialog) {
        return model;
    }

    @Override
    public CreateRoomModel getModel(PrivacyDialog dialog) {
        return model;
    }

    @Override
    public CreateRoomModel getModel(PickCommunityDialog dialog) {
        return model;
    }

    @Override
    public ArrayAdapter<Group> getAdapter(PickCommunityDialog dialog) {
        return communityAdapter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        requestQueue.cancelAll(this);
    }

    private void submit() {
        try {
            JSONObject jsonObject = model.toJSON();

            //noinspection ConstantConditions toJSON will have thrown if community is null
            String groupId = model.getCommunity().getId();
            setSubmitting(true);

            requestQueue.add(new ApiRequest<String>(this, Request.Method.POST, "/v1/groups/" + groupId + "/rooms", jsonObject,
               new Response.Listener<String>() {
                @Override
                public void onResponse(String id) {
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(RESULT_EXTRA_ROOM_ID, id);
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    setSubmitting(false);
                    showError(R.string.create_room_failed);
                }
            }) {
                @Override
                protected String parseJsonInBackground(String json) throws JSONException {
                    return new JSONObject(json).getString("id");
                }
            });
        } catch (CreateRoomModel.CommunityMissingException e) {
            showError(R.string.create_room_community_required);
        } catch (CreateRoomModel.RoomNameMissingException e) {
            showError(R.string.create_room_room_name_required);
        } catch (JSONException e) {
            showError(R.string.create_room_failed);
        }

    }

    private void setSubmitting(boolean isSubmitting) {
        this.isSubmitting = isSubmitting;
        if (isSubmitting) {
            creatingSnackbar.show();
        } else {
            creatingSnackbar.dismiss();
        }
        invalidateOptionsMenu();
    }

    private void showError(@StringRes int messageResource) {
        Snackbar.make(rootView, messageResource, Snackbar.LENGTH_SHORT).show();
    }
}
